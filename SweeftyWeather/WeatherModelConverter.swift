//
//  WeatherModelConverter.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 22/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import Foundation

extension NSCalendar {
    func sameDays(date: NSDate,otherDate: NSDate) -> Bool {
        return self.isDate(date, equalToDate: otherDate, toUnitGranularity: .Day)
    }
}

func hasDateOfTheSameDay(calendar: NSCalendar, date: NSDate, days: [WeatherViewable]) -> Bool {
    return days.filter{ weather in
        return calendar.sameDays(date, otherDate: weather.time)
        }.count > 0
}

protocol WeatherViewable {
    var time: NSDate { get }
    var temperature: Double { get }
}

extension Weather : WeatherViewable {
    
}

extension Array where Element:WeatherViewable {
    func filterToOneResultPerDay(calendar: NSCalendar) -> [WeatherViewable] {
        var filtered = [WeatherViewable]()
        
        for w in self {
            if !hasDateOfTheSameDay(calendar, date: w.time, days: filtered) {
                filtered.append(w)
            }
        }
        
        return filtered
    }
}

class WeatherModelConverter {
    
    let df = FormatterHelper().dateFormatter(.MediumStyle,timeStyle: .NoStyle)
    let calendar = NSCalendar.currentCalendar()
    
    func convertToViewModel(forecast: Forecast) -> [WeatherViewModel] {
        return forecast.weather.filterToOneResultPerDay(calendar).map({ weather in
            return WeatherViewModel(date: df.stringFromDate(weather.time),
                temprerature: "\(weather.temperature)℃")
        })
    }
    
    
}