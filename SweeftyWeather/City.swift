//
//  City.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import CoreLocation

struct City {
    let name: String
    let id: Int
    let coordinate: CLLocationCoordinate2D
    let countryCode: String
    
    init(name: String, id: Int, coordinate: CLLocationCoordinate2D, countryCode: String) {
        self.name = name
        self.id = id
        self.coordinate = coordinate
        self.countryCode = countryCode
    }
}
