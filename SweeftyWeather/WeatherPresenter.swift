//
//  WeatherPresenter.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 19/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import Foundation

enum WeatherModel {
    case LocationError(String)
    case WeatherError(String)
    case Weather(Forecast)
}

class WeatherPresenter {
    let location: LocationManager
    let cityManager: CityManager
    let weatherManager: WeatherManager
    
    init (location : LocationManager,
          cityManager : CityManager,
          weatherManager : WeatherManager) {
        self.location = location
        self.cityManager  = cityManager
        self.weatherManager = weatherManager
    }
    
    func fetchWeather(completion: (weatherModel: WeatherModel) -> Void) {
        fetchCurrentCity {[weak self] (city) in
            guard let city = city else {
                completion(weatherModel: .LocationError("Can't get location"))
                return
            }
            self?.fetchForecast(city, completion: { (forecast) in
                guard let fcast = forecast else {
                    completion(weatherModel: .WeatherError("Can't get forecast for \(city.name)"))
                    return
                }
                dispatch_async(dispatch_get_main_queue(), { 
                    completion(weatherModel: .Weather(fcast))
                })
            })
        }
    }
    
    func fetchCurrentCity(completion: (city: City?) -> Void) {
        location.askForLocation {[weak self] (allowed) -> () in
            
            guard allowed else {
                completion(city: nil)
                return
            }
            
            self?.location.getLocation {[weak self] (coordinate) -> () in
                
                self?.cityManager.getCityAtLocation(coordinate, completion: { (city) -> () in
                    completion(city: city)
                })
            }
        }
    }
    
    func fetchForecast(city: City, completion: (forecast: Forecast?) -> Void) {
        weatherManager.forecast(city, completion: { fcast in
            completion(forecast: fcast)
        })
    }
}




