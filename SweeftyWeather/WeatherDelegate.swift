//
//  WeatherLayout.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 19/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import UIKit

class WeatherDelegate : NSObject, UICollectionViewDelegate{
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        switch width {
        case 320...414:
            return CGSize(width: width,
                          height: 90)
        case 414...507:
            return CGSize(width: floor(width/2.0)-0.5,
                          height: 200)
        case 507...768:
            return CGSize(width: floor(width/2.0)-0.5,
                          height: 200)
        default:
            return CGSize(width: floor(width/3.0)-1.0,
                          height: 350)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
}