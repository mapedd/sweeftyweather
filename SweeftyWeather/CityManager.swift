//
//  CityManager.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import CoreLocation

class CityManager {
    
    var coder = CLGeocoder()
    
    func getCityAtLocation(location:CLLocationCoordinate2D, completion:((City?)->())){
        
        if coder.geocoding {
            completion(nil)
            return
        }
        
        let loc = CLLocation(
            latitude: location.latitude,
            longitude: location.longitude)
        
        coder.reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) -> Void in
            guard let placemarks = placemarks where placemarks.count > 0 else {
                completion(nil)
                return
            }
            
            guard let placemark = placemarks[0] as CLPlacemark? else {
                completion(nil)
                return 
            }
            
            let city = City(name: placemark.locality!, id: 0, coordinate: location, countryCode: placemark.ISOcountryCode!)
            completion(city)
            
        })
    }
}
