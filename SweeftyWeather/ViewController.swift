//
//  ViewController.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 27/07/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var dataSource: WeatherDataSource!
    let delegate: WeatherDelegate
    let presenter: WeatherPresenter
    var collectionView: UICollectionView!
    
    init(presenter: WeatherPresenter) {
        self.presenter = presenter
        delegate = WeatherDelegate()
        super.init(nibName: nil, bundle: nil)
        title = "Local weather"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    func newCollectionView() -> UICollectionView {
        let collectionView = UICollectionView(frame: view.bounds,
                                              collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = UIColor.grayColor()
        collectionView.alwaysBounceVertical = true
        collectionView.autoresizingMask = [.FlexibleWidth,.FlexibleHeight]
        return collectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Refresh", style: .Plain, target: self, action: #selector(ViewController.loadData)
        )
        collectionView = newCollectionView()
        
        dataSource = WeatherDataSource(collectionView: collectionView,
                                       presenter: presenter)
        collectionView.dataSource = dataSource
        collectionView.delegate = delegate
        view.addSubview(collectionView)
    }
    
    func loadData() {
        dataSource.loadData()
    }
}