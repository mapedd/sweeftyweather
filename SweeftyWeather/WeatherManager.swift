//
//  WeatherManager.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit

class WeatherManager {
    
    let base = NSURL(string:"http://api.openweathermap.org/data/2.5/forecast")!
    let network: WeatherNetwork
    let mapper = ForecastMapper()
    
    init(network: WeatherNetwork) {
        self.network = network
    }
    
    func forecast(city: City, completion: ((Forecast?) -> ())){
        let comps = NSURLComponents(URL: base, resolvingAgainstBaseURL: false)!
        let item = NSURLQueryItem(name: "q", value: "\(city.name),\(city.countryCode)")
        comps.queryItems = [item]
        
        network.GET(comps.URL!, completion: {[weak self] (data, error) -> () in
            guard let data = data else {
                completion(nil)
                return
            }
            let fcast = self?.mapper.mapFromData(data)
            completion(fcast)
        })
    }
}
