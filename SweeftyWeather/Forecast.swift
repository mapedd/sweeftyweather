//
//  Forecast.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation

struct Forecast {
    let city: City
    let weather: [Weather]
    init(city:City, weather:[Weather]) {
        self.city = city
        self.weather = weather
    }
}