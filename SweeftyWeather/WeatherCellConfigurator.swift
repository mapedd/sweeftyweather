//
//  WeatherCellConfigurator.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 22/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import Foundation

class WeatherCellConfigurator {
    func configure(cell: WeatherCell, viewModel: WeatherViewModel) {
        cell.labelDate.text = viewModel.date
        cell.labelTemp.text = viewModel.temprerature
    }
}