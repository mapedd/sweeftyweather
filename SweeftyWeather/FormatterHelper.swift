//
//  FormatterHelper.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit

class FormatterHelper {

    private func currentThreadFormatters() -> [String:NSDateFormatter] {
        let thread = NSThread.currentThread()
        
        var formatters = thread.threadDictionary["formatters"] as? [String:NSDateFormatter]
        
        if formatters == nil {
            formatters = [String:NSDateFormatter]()
            thread.threadDictionary["formatters"] = formatters
        }
     
        return formatters!
    }
    
    private func storeAndReturnFormatter(key: String, configuration: (df: NSDateFormatter) -> Void) -> NSDateFormatter {
        var forms = currentThreadFormatters()
        
        var formatter = forms[key] as NSDateFormatter?
        
        if formatter == nil {
            formatter = NSDateFormatter()
            configuration(df: formatter!)
            forms[key] = formatter
        }
        
        return formatter!
    }
    
    func dateFormatter(dateStyle: NSDateFormatterStyle, timeStyle: NSDateFormatterStyle) -> NSDateFormatter {
        
        let key = "__\(dateStyle.rawValue)+\(timeStyle.rawValue)"
        
        return storeAndReturnFormatter(key) { (df) in
            df.dateStyle = dateStyle
            df.timeStyle = timeStyle
        }
    }
    
    func dateFormatter(format: String) -> NSDateFormatter {
        return storeAndReturnFormatter(format, configuration: { (df) in
             df.dateFormat = format
        })
        
    }
   
}
