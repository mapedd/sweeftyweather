//
//  AppDelegate.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let viewController = ViewController(presenter: createPresenter())
        let nc = UINavigationController(rootViewController: viewController)
        window!.rootViewController = nc
        window!.makeKeyAndVisible()
        return true
    }
    
    func createPresenter() -> WeatherPresenter {
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        let network = WeatherNetwork(session: session)
        let weatherManager = WeatherManager(network: network)
        let locationManager = CLLocationManager()
        return WeatherPresenter(location: LocationManager(manager:locationManager),
                                cityManager: CityManager(),
                                weatherManager: weatherManager)
        
    }
}

