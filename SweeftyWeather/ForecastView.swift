//
//  ForecastView.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit

class ForecastView: UIView {

    var descriptionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = UIColor.whiteColor()
        descriptionLabel = UILabel(frame: bounds)
        descriptionLabel.autoresizingMask = [.FlexibleWidth,.FlexibleHeight]
        let font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        descriptionLabel.font = font.scaled(3.0)
        descriptionLabel.textAlignment = .Center
        descriptionLabel.numberOfLines = 0
        addSubview(descriptionLabel)
    }
}
