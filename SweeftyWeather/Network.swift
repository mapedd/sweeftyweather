//
//  Network.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit

class Network {
    
    let session: NSURLSession
    
    init(session: NSURLSession) {
        self.session = session
    }
    
    func GET(url:NSURL, completion:((data:NSData?, error:NSError?)->())) -> NSURLSessionDataTask?{
        let task = session.dataTaskWithURL(url, completionHandler: { (d, r, e) -> Void in
            completion(data:d,error:e)
        })
        task.resume()
        return task
    }
}
