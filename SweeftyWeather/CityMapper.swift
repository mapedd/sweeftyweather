//
//  CityMapper.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import CoreLocation

class CityMapper {
    
    func mapFromData(data: [String:AnyObject]) -> City?{
        
        let name = data["name"] as? String
        let id = data["id"] as? Int
        let countryCode = data["country"] as? String
        let coordinates = data["coord"] as? [String:Double]
        var location: CLLocationCoordinate2D
        if let coord = coordinates {
            location = CLLocationCoordinate2D()
            if let lon = coord["lon"] {
                location.longitude = lon
            } else {return nil}
            
            if let lat = coord["lat"] {
                location.latitude = lat
            } else {return nil}
            
        } else {
            return nil
        }
        
        if name == nil || id == nil || countryCode == nil {
            return nil
        }
        
        return City(
            name: name!,
            id: id!,
            coordinate:location,
            countryCode: countryCode!)
    }
}
