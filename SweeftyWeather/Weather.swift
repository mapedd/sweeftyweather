//
//  Weather.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 27/07/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation

struct Weather {
    init( time: NSDate,
          mainDescription: String,
          rain3h: Double?,
          wind: Double,
          temperature: Double,
          temperatureMin: Double,
          temperatureMax: Double) {
        self.time = time
        self.mainDescription = mainDescription
        self.rain3h = rain3h
        self.wind = wind
        self.temperature = temperature
        self.temperatureMin = temperatureMin
        self.temperatureMax = temperatureMax
    }
    let time: NSDate
    let mainDescription: String
    let rain3h: Double?
    let wind: Double
    
    let temperature: Double
    let temperatureMin: Double
    let temperatureMax: Double
}


extension Weather: Equatable {
    
}

func ==(lhs: Weather, rhs: Weather) -> Bool {
    if lhs.time != rhs.time {
        return false
    }
    if lhs.mainDescription != rhs.mainDescription {
        return false
    }
    if lhs.rain3h != rhs.rain3h {
        return false
    }
    if lhs.wind != rhs.wind {
        return false
    }
    if lhs.temperature != rhs.temperature {
        return false
    }
    if lhs.temperatureMin != rhs.temperatureMin {
        return false
    }
    if lhs.temperatureMax != rhs.temperatureMax {
        return false
    }
    
    return true
}