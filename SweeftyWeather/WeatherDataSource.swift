//
//  File.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 19/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import UIKit

class WeatherDataSource : NSObject, UICollectionViewDataSource {
    let collectionView : UICollectionView
    let presenter: WeatherPresenter
    let converter = WeatherModelConverter()
    let configurator = WeatherCellConfigurator()
    
    init (collectionView: UICollectionView, presenter: WeatherPresenter) {
        self.collectionView = collectionView
        self.presenter = presenter
        super.init()
        self.collectionView.dataSource = self
        self.collectionView.registerClass(WeatherCell.self,
                                           forCellWithReuseIdentifier: WeatherCell.id())
    }
    
    func loadData() {
        self.presentMessage("Loading...")
        presenter.fetchWeather {[weak self] (weatherModel) in
            assert(NSThread.isMainThread())
            switch weatherModel {
            case .Weather(let forecast) :
                self?.presentForecast(forecast)
            case .LocationError(let description) :
                self?.presentMessage(description)
            case .WeatherError(let description) :
                self?.presentMessage(description)
            }
        }
    }
    
    private var fetchedViewModels = [WeatherViewModel]()
    
    func presentForecast(forecast: Forecast) {
        forecastView.removeFromSuperview()
        fetchedViewModels = converter.convertToViewModel(forecast)
        collectionView.reloadData()
    }
    
    lazy private var forecastView: ForecastView = {
        let view = ForecastView(frame: CGRect(origin: CGPointZero,
            size: self.collectionView.frame.size))
        view.autoresizingMask = [.FlexibleHeight,.FlexibleWidth]
        return view
    }()
    
    func presentMessage(description: String) {
        fetchedViewModels = []
        forecastView.frame = CGRect(origin: CGPointZero,
                                    size: collectionView.frame.size)
        collectionView.superview?.addSubview(forecastView)
        forecastView.descriptionLabel.text = description
        collectionView.reloadData()
    }
    
    @objc func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedViewModels.count
    }
    
    @objc func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(WeatherCell.id(), forIndexPath: indexPath) as? WeatherCell
        let viewModel = fetchedViewModels[indexPath.item]
        configurator.configure(cell!, viewModel: viewModel)
        return cell!
    }
}