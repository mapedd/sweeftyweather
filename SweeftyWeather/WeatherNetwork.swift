//
//  WeatherNetwork.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit

class WeatherNetwork: Network {
    private let appId = "2b4d90261d355e3ffb0ecbd20cc78251"
    private let appIdKey = "APPID"
    
    override func GET(url: NSURL, completion: ((data: NSData?, error: NSError?) -> ())) -> NSURLSessionDataTask? {
        guard let urlComponents = NSURLComponents(URL: url, resolvingAgainstBaseURL: false) else {
            completion(data: nil, error: nil)
            return nil
        }
        
        let appIdItems:[NSURLQueryItem] = [
            NSURLQueryItem(name: appIdKey, value: appId),
            NSURLQueryItem(name:"units", value:"metric")
        ]
        
        if var items = urlComponents.queryItems {
            items.appendContentsOf(appIdItems)
            urlComponents.queryItems = items
        } else {
            urlComponents.queryItems = appIdItems
        }
        
        return super.GET(urlComponents.URL!, completion: completion)
    }
}
