//
//  ForecastMapper.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation

class ForecastMapper {
    
    let weatherMapper = WeatherMapper()
    let cityMapper = CityMapper()
    
    func mapFromData(data:NSData) -> Forecast?{
        let jsonOptional = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as! [String:AnyObject]?
        
        guard let json = jsonOptional else {
            return nil
        }
        
        if json["cod"] as? String != "200" {
            return nil
        }
        
        let cityData = json["city"] as! [String:AnyObject]?
        if cityData == nil {
            return nil
        }
        let city = cityMapper.mapFromData(cityData!)
        if city == nil {
            return nil
        }
        
        let weatherData = json["list"] as! [[String:AnyObject]]?
        if weatherData == nil {
            return nil
        }
        if weatherData!.count == 0 {
            return nil
        }
        
        var weathers = [Weather]()
        
        for dict in weatherData! {
            let weather = weatherMapper.mapFromData(dict)
            if weather != nil {
                weathers.append(weather!)
            }
        }
        
        let forecast = Forecast(city: city!, weather: weathers)
        
        return forecast
    }
}