//
//  WeatherMapper.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation

class WeatherMapper {
    
    let dateFormatter = FormatterHelper().dateFormatter("yyyy-MM-dd HH:mm:ss")
    
    func mapFromData(data: [String:AnyObject]) -> Weather?{
        
        guard let dtTxt = data["dt_txt"] as? String else {
            return nil
        }
        
        let date = dateFormatter.dateFromString(dtTxt)
        
        guard let main = data["main"] as? [String:Double] else {
            return nil
        }
        
        guard let temp = main["temp"],
            let tempMin = main["temp_min"],
            let tempMax = main["temp_max"] else {
                return nil
        }
        
        guard let windDict = data["wind"] as? [String:Double],
            let windSpeed = windDict["speed"] else {
                return nil
        }
        
        let rainDict = data["rain"] as? [String:Double]
        let rain = rainDict?["3h"] as Double?
        
        guard let desc = data["weather"] as? [[String:AnyObject]] where desc.count > 0,
            let description = desc[0]["description"] as? String else {
                return nil
        }
        
        return Weather(time: date!,
                       mainDescription: description,
                       rain3h: rain,
                       wind: windSpeed,
                       temperature: temp,
                       temperatureMin: tempMin,
                       temperatureMax: tempMax)
    }
    
    func mapFromData(data: NSData) -> Weather? {
        let object: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(data, options: [])
        if let object = object as? [String:AnyObject]{
            return mapFromData(object)
        } else {
            return nil
        }
    }
}
