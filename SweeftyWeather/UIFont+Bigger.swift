//
//  UIFont+Bigger.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 22/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import UIKit

extension UIFont {
    func scaled(scale: CGFloat) -> UIFont {
        let fontDescriptor = self.fontDescriptor()
        return UIFont(descriptor: fontDescriptor, size: self.pointSize * scale)
    }
}