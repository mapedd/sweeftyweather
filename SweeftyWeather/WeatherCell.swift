//
//  WeatherCell.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 19/08/2016.
//  Copyright © 2016 SweeftApps. All rights reserved.
//

import UIKit

class WeatherCell: UICollectionViewCell {
    class func id() -> String {
        return "WeatherCell"
    }
    
    let labelDate: UILabel
    let labelTemp: UILabel
    
    override init(frame: CGRect) {
        labelDate = UILabel(frame: CGRectZero)
        labelTemp = UILabel(frame: CGRectZero)
        super.init(frame: frame)
        contentView.backgroundColor = UIColor.whiteColor()
        contentView.addSubview(labelDate)
        contentView.addSubview(labelTemp)
        let tempFont = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        labelTemp.font = tempFont.scaled(3.0)
        let dateFont = UIFont.preferredFontForTextStyle(UIFontTextStyleCaption2)
        labelDate.font = dateFont.scaled(2.0)
        labelDate.textAlignment = .Center
        labelDate.adjustsFontSizeToFitWidth = true
        labelTemp.textAlignment = .Center
        labelTemp.adjustsFontSizeToFitWidth = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let rect = self.contentView.bounds
        labelDate.frame = CGRect(origin: CGPointZero,
                                 size: CGSize(width: rect.size.width,
                                    height: rect.size.height/3.0))
        labelTemp.frame = CGRect(origin: CGPoint(x: 0, y:CGRectGetMaxY(labelDate.frame)),
                                 size: CGSize(width: rect.size.width,
                                    height: 2.0*rect.size.height/3.0))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}