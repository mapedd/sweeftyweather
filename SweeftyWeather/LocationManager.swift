//
//  LocationManager.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager : NSObject,CLLocationManagerDelegate{
    let manager: CLLocationManager
    
    init(manager: CLLocationManager) {
        self.manager = manager
        super.init()
        self.manager.delegate = self
    }
    
    private var permission: ((allowed: Bool)->())?
    private var locationCompletion: ((location: CLLocationCoordinate2D)->())?
    
    func askForLocation(completion: ((allowed: Bool)->())) {
        
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .Restricted, .Denied:
            completion(allowed: false)
        case .NotDetermined:
            self.permission = completion
            askUserToAllow()
        case .AuthorizedAlways,.AuthorizedWhenInUse:
            completion(allowed: true)
        }
    }
    func getLocation(completion: ((location: CLLocationCoordinate2D) -> ())) {
        locationCompletion = completion
        manager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        manager.startUpdatingLocation()
    }
    
    func askUserToAllow() {
        manager.requestWhenInUseAuthorization()
    }
    
    //MARK - CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            manager.stopUpdatingLocation()
            let first = locations[0]
            locationCompletion?(location: first.coordinate)
            
        }
    }
    
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            
            switch status {
            case .Restricted, .Denied:
                permission?(allowed: false)
            case .NotDetermined:
                askUserToAllow()
            case .AuthorizedAlways,.AuthorizedWhenInUse:
                permission?(allowed: true)
            }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
    }
}
