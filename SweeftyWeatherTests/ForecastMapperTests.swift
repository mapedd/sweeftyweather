//
//  ForecastMapperTests.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import XCTest
@testable import SweeftyWeather

class ForecastMapperTests: XCTestCase {
    
    func testMapperProperlyParsesValidData(){
        let mapper = ForecastMapper()
        let data = sampleRawData(ForecastMapperTests.self, fileName: "london_weather.json")
        let fcast = mapper.mapFromData(data!)
        XCTAssertNotNil(fcast)
        XCTAssertNotNil(fcast!.city)
        XCTAssertNotNil(fcast!.weather)
        XCTAssertEqual(fcast!.weather.count,40)
    }
    
    func testMapperReturnNilForUnvalidData() {
        let mapper = ForecastMapper()
        let data = sampleRawData(ForecastMapperTests.self, fileName: "london_broken_file.json")
        let fcast = mapper.mapFromData(data!)
        XCTAssertNil(fcast)
    }
}
