//
//  CityMapperTests.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import XCTest
@testable import SweeftyWeather

class CityMapperTests: XCTestCase {
    
    func testDoesntMapFromDataWithNoValues() {
        let data = [String:AnyObject]()
        XCTAssertNil(CityMapper().mapFromData(data))
    }
    
    func testDoesntMapFromDataWithMissingValues() {
        let data: [String:AnyObject] = [
            "name" : "A name",
            "id"  : 24123
        ]
        XCTAssertNil(CityMapper().mapFromData(data))
    }
    
    func testMapsCityFromCorrectData() {
        let data = sampleValidData()
        XCTAssertNotNil(CityMapper().mapFromData(data))
    }
    
    func testMappingCorrectDataCreatesCorrectCityObject() {
        let data = sampleValidData()
        let city = CityMapper().mapFromData(data)!
        XCTAssertEqual(city.name, "Garwolin")
        XCTAssertEqual(city.id, 772339)
        XCTAssertEqual(city.countryCode, "PL")
        XCTAssertEqual(city.coordinate.latitude, Double(51.897469))
        XCTAssertEqual(city.coordinate.longitude, Double(21.61466))
        
    }
    
    func sampleValidData() -> [String:AnyObject] {
        let url = NSBundle(forClass:CityMapperTests.self).URLForResource("sample_city",
                                                                         withExtension: "json")
        let data = NSData(contentsOfURL: url!)!
        return (try? NSJSONSerialization.JSONObjectWithData(data,
            options: [])) as! [String:AnyObject]!
    }
    
}
