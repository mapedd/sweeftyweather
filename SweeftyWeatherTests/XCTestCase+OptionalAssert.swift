//
//  XCTestCase+OptionalAssert.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    
    func SAAssertEqualOptional<T : Equatable>(@autoclosure theOptional:  () -> T?, @autoclosure _ expression2:  () -> T?, file: String = #file, line: UInt = #line) {
        
        if let e = theOptional() {
            let e2 = expression2()
            if e != e2 {
                self.recordFailureWithDescription("Optional (\(e)) is not equal to (\(e2))", inFile: file, atLine: line, expected: true)
            }
        }
        else {
            self.recordFailureWithDescription("Optional value is empty", inFile: file, atLine: line, expected: true)
        }
    }
}