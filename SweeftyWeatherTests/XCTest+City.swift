//
//  XCTest+City.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation
import XCTest
@testable import SweeftyWeather
import CoreLocation

extension XCTestCase {
    func london() -> City {
        return City(name: "London", id: 0, coordinate: CLLocationCoordinate2D(), countryCode: "uk")
    }
}