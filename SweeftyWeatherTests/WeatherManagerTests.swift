//
//  WeatherManagerTests.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import CoreLocation
import XCTest
@testable import SweeftyWeather

class TestNetwork : WeatherNetwork {
    var url: NSURL?
    var data: NSData?
    var error: NSError?
    
    override func GET(url: NSURL, completion: ((data: NSData?, error: NSError?) -> ()))->NSURLSessionDataTask {
        self.url = url
        completion(data: data, error: error)
        return NSURLSessionDataTask()
    }
}

class WeatherManagerTests: XCTestCase {
    
    var manager: WeatherManager!
    var network: TestNetwork!
    override func setUp() {
        super.setUp()
        
        network = TestNetwork(session: TestSession())
        manager = WeatherManager(network: network)
    }
    
    func testManagerCallCorrectURLWithParameters() {
        
        manager.forecast(london(), completion: { weather in
            
        })
        SAAssertEqualOptional(network.url, NSURL(string: "http://api.openweathermap.org/data/2.5/forecast?q=London,uk"))
    }
    
    func testManagerReturnWeatherWhenNetowrkReturnCorrectData() {
        let data = sampleRawData(WeatherManagerTests.self, fileName: "london_weather.json")
        network.data = data
        let e = expectationWithDescription("weather manager")
        manager.forecast(london(), completion: { forecast in
            XCTAssertNotNil(forecast)
            e.fulfill()
        })
        waitForExpectationsWithTimeout(0.01, handler: nil)
    }
    
    func testManagerReturnsNilWhenNetworkReturnError() {

        network.error = NSError(domain: "domain", code: 123, userInfo: nil)
        let e = expectationWithDescription("weather manager")
        manager.forecast(london(), completion: { forecast in
            XCTAssertNil(forecast)
            e.fulfill()
        })
        waitForExpectationsWithTimeout(0.01, handler: nil)
    }
    
    func testManagerReturnNilWhenNetworkReturnsWrongFormatData() {
        let data = sampleRawData(WeatherManagerTests.self, fileName: "sample_weather_no_temperature.json")
        network.data = data
        let e = expectationWithDescription("weather manager")
        manager.forecast(london(), completion: { forecast in
            XCTAssertNil(forecast)
            e.fulfill()
        })
        waitForExpectationsWithTimeout(0.01, handler: nil)
    }
}
