//
//  XCTestCase+DateAssert.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    
    func SAAssertEqualDateWithFormat(@autoclosure date:   () -> NSDate,@autoclosure dateString:  () -> String, @autoclosure format:  () -> String , file: String = #file, line: UInt = #line) {
        if !date().equalWithFormat(dateString(), format:format()) {
            recordFailureWithDescription("Date  (\(date())) is not equal to (\(dateString()))", inFile: file, atLine: line, expected: true)
        }
    }
}