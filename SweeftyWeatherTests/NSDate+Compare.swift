//
//  NSDate+Compare.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation


extension NSDate {
    
    func equalWithFormat(dateString: String, format: String) -> Bool {
        let calendar = NSCalendar.currentCalendar()
        let df = NSDateFormatter()
        df.dateFormat = format
        let compareDate =  df.dateFromString(dateString)
        if compareDate == nil {
            
            return false
        }
        
        let compare = compareDate!
        
        let flags : NSCalendarUnit = (
            [.Year, .Month, .Day, .Hour, .Minute, .Second]
        )
        
        
        let comps = calendar.components(flags, fromDate: compare, toDate: self, options: [])
        
        let yearEqual: Bool = format.contains("yyyy") ? comps.year == 0 : true
        let monthEqual: Bool = format.contains("MM") ? comps.month == 0 : true
        let dayEqual: Bool = format.contains("dd") ? comps.day == 0 : true
        let hourEqual: Bool = format.contains("HH") ? comps.hour == 0 : true
        let minuteEqual: Bool = format.contains("mm") ? comps.minute == 0 : true
        let secondEqual: Bool = format.contains("ss") ? comps.second == 0 : true
        
        let all =  yearEqual && monthEqual && dayEqual && hourEqual && minuteEqual && secondEqual
        
        return all
        
    }
}