//
//  NetworkTests.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import XCTest
@testable import SweeftyWeather

class TestTask : NSURLSessionDataTask {
    var resumed = false
    override func resume() {
        resumed = true
    }
}

class TestSession : NSURLSession {
    var url : NSURL?
    var dataToReturn: NSData?
    var errorToReturn: NSError?
    override func dataTaskWithURL(url: NSURL, completionHandler: ((NSData?, NSURLResponse?, NSError?) -> Void)) -> NSURLSessionDataTask {
        
        self.url = url
        completionHandler(dataToReturn,NSURLResponse(),errorToReturn)
        return TestTask()
    }
}

class NetworkTests: XCTestCase {
    
    var network: Network!
    var session: TestSession!
    
    override func setUp() {
        
        session = TestSession()
        session = TestSession()
        network = Network(session: session)
        super.setUp()
    }
    
    func testCallingGETPassesURLToSession() {
        network.GET(NSURL(string:"http://www.ebay.co.uk")!, completion: { (data, error) -> () in })
        SAAssertEqualOptional(NSURL(string:"http://www.ebay.co.uk"),session.url)
    }
    
    func testCallingGETGeneratesTaskAndCallResumseOnIn() {
        let task = network.GET(NSURL(string:"http://www.ebay.co.uk")!, completion: { (data, error) -> () in })
        let aTask = task as! TestTask
        XCTAssertTrue(aTask.resumed)
    }
    
    func testCompletionIsCalledInGETAndDataAndErrorArePassingFromSession() {
        session.dataToReturn = "hello world".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        session.errorToReturn = NSError(domain: "domain", code: 123, userInfo: nil)
        let e = expectationWithDescription("completion callback")
        network.GET(NSURL(string:"http://www.ebay.co.uk")!, completion: { (data, error) -> () in
            XCTAssertEqual(data, "hello world".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false))
            XCTAssertEqual(error!.domain, "domain")
            XCTAssertEqual(error!.code, 123)
            e.fulfill()
        })
        waitForExpectationsWithTimeout(0.01, handler: nil)
    }
}
