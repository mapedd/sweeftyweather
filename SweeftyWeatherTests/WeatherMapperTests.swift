//
//  SweeftyWeatherTests.swift
//  SweeftyWeatherTests
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import UIKit
import XCTest
@testable import SweeftyWeather

class WeatheMapperTests: XCTestCase {
    
    func testWeatherMapperReturnNilIfInputDataIsEmpty() {
        XCTAssertNil(WeatherMapper().mapFromData([String : AnyObject]()))
    }
    
    func testWeatherMapperReturnsNilIfMissingDate() {
        XCTAssertNil(WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather_no_date.json")!))
    }
    
    func testWeatherMapperReturnsNilIfMissingTemperature() {
        XCTAssertNil(WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather_no_temperature.json")!))
    }
    
    func testWeatherMapperReturnsNonNilObjectForCorrectData() {
        XCTAssertNotNil(WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather.json")!))
    }
    
    func testWeatherMapperReturnsWeatherObjectWithCorrectlyParsedDate() {
        let weather = WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather.json")!)
        SAAssertEqualDateWithFormat(weather!.time,
            dateString: "2016-07-28 12:23:11",
            format: "yyyy-MM-dd HH:mm:ss")
    }
    
    func testWeatherMapperReturnsWeatherObjectWithParsedTemperature() {
        let weather = WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather.json")!)
        XCTAssertEqual(weather!.temperature, 13.67)
    }
    
    func testWeatherMapperReturnsWeatherObjectWithParsedRain() {
        let weather = WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather.json")!)
        XCTAssertEqual(weather!.rain3h, 3.58)
    }
    
    func testWeatherMapperReturnsWeatherObjectWithParsedWind() {
        let weather = WeatherMapper().mapFromData(sampleData(WeatheMapperTests.self, fileName: "sample_weather.json")!)
        XCTAssertEqual(weather!.wind, 3.96)
    }
    
    func testWeatherMapperParsesNSDataFromJSONFormat() {
        let data = sampleRawData(WeatherNetworkTests.self, fileName: "sample_weather.json")
        
        let weather = WeatherMapper().mapFromData(data!)
        
        XCTAssertNotNil(weather)
    }
}
