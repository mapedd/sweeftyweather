//
//  WeatherNetworkTests.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//


import XCTest
@testable import SweeftyWeather

class WeatherNetworkTests: XCTestCase {
    var network: WeatherNetwork!
    var session: TestSession!
    
    override func setUp() {
        super.setUp()
        session = TestSession()
        network = WeatherNetwork(session: session)
    }
    
    func testGETPassesAppIdToSession() {
        network.GET(NSURL(string: "http://www.ebay.co.uk")!, completion: { (data, error) -> () in
            
        })
        let comps = NSURLComponents(URL: session.url!, resolvingAgainstBaseURL: false)
        let keys = comps!.queryItems!.filter { item in
            return item.name.contains("APPID")
        }
        XCTAssertEqual(keys.count, 1)
    }
    
    func testGETPassesMetricUnitsToSession() {
        network.GET(NSURL(string: "http://www.ebay.co.uk")!, completion: { (data, error) -> () in
            
        })
        let comps = NSURLComponents(URL: session.url!, resolvingAgainstBaseURL: false)
        let keys = comps!.queryItems!.filter { item in
            return item.name == "units"
        }
        XCTAssertEqual(keys.count, 1)
        let item = keys[0] 
        XCTAssertEqual(item.name, "units")
        XCTAssertEqual(item.value!, "metric")
    }


}
