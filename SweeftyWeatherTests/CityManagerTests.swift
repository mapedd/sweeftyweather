//
//  CityManagerTests.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

@testable import SweeftyWeather
import XCTest
import CoreLocation

class TestGeocoder : CLGeocoder {
    var geoing = false
    
    override var geocoding: Bool {
        get {
            return geoing
        }
    }
    
    var location: CLLocation?
    var placemarks: [CLPlacemark]?
    var error: NSError?
    
    override func reverseGeocodeLocation(location: CLLocation, completionHandler: CLGeocodeCompletionHandler) {
        self.location = location
        completionHandler(placemarks,error)
    }
    
}

class TestPlacemark: CLPlacemark {
    var localityToReturn:String?
    var countryCodeToReturn:String?
    override var locality: String? {
        get{
            return localityToReturn!
        }
    }
    override var ISOcountryCode: String? {
        get{
            return countryCodeToReturn!
        }
    }
    
}

class CityManagerTests: XCTestCase {
    
    var manager: CityManager!
    var geocoder: TestGeocoder!

    override func setUp() {
        super.setUp()
        manager = CityManager()
        geocoder = TestGeocoder()
        manager.coder = geocoder!
    }

    func testCityForLocationPassesCoordinateToCoder() {
        var loc = CLLocationCoordinate2D()
        loc.longitude = 123.0
        loc.latitude = 0.122
        manager.getCityAtLocation(loc, completion: { city in
            
        })
        let receivedLoc = geocoder!.location!
        XCTAssertEqual(receivedLoc.coordinate, loc)
    }
    
    func testCityForLocationDoesntSpawnSecondRequest() {
        var loc = CLLocationCoordinate2D()
        loc.longitude = 123.0
        loc.latitude = 0.122
        manager.getCityAtLocation(loc, completion: { city in
            
        })
        geocoder!.geoing = true
        loc.longitude = 1.0
        loc.latitude = 1.0
        manager.getCityAtLocation(loc, completion: { city in
            
        })
        var expected = CLLocationCoordinate2D()
        expected.longitude = 123.0
        expected.latitude = 0.122
        let receivedLoc = geocoder.location!
        XCTAssertEqual(receivedLoc.coordinate, expected)
    }
    
    func testCityForLocationReturnsCityMadeFromFirstResultOfGeocoding() {
        let loc = CLLocationCoordinate2D()
        let placemark = TestPlacemark()
        placemark.localityToReturn = "London"
        placemark.countryCodeToReturn = "UK"
        geocoder.placemarks = [placemark]
        let e = expectationWithDescription("city from manager")
        manager.getCityAtLocation(loc, completion: { city in
            XCTAssertEqual(city!.name,"London")
            XCTAssertEqual(city!.countryCode, "UK")
            e.fulfill()
        })
        waitForExpectationsWithTimeout(0.01, handler: nil)
        
    }
    
    func testCityForLocationReurnsNilWhenNoCityIfFound() {
        let loc = CLLocationCoordinate2D()
        geocoder.placemarks = []
        let e = expectationWithDescription("city from manager")
        manager.getCityAtLocation(loc, completion: { city in
            XCTAssertNil(city)
            e.fulfill()
        })
        waitForExpectationsWithTimeout(0.01, handler: nil)
    }
}
