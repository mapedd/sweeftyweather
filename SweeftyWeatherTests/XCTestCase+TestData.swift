//
//  XCTestCase+TestData.swift
//  SweeftyWeather
//
//  Created by Tomasz Kuzma on 15/08/2016.
//  Copyright (c) 2016 SweeftApps. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    func sampleData(aClass: AnyClass, fileName:String) -> [String:AnyObject]? {
        
        let validData = sampleRawData(aClass, fileName: fileName)
        
        if validData == nil {
            return nil
        }
        
        let sample = (try? NSJSONSerialization.JSONObjectWithData(validData!,
            options: [])) as! [String:AnyObject]?

        return sample
    }
    
    func sampleRawData(aClass: AnyClass, fileName:String) -> NSData? {
        let bundle = NSBundle(forClass: aClass)
        let url = bundle.URLForResource(fileName, withExtension: nil)
        if url == nil {
            return nil
        }
        
        let validUrl = url!
        
        let data = NSData(contentsOfURL: validUrl)
        
        return data
    }
}