##SweeftyWeather v0.001

![image](screenshot.png)

1. Build instructions
	* Open scheme preferences in your Xcode with `⇧+⌘+,`
	* Enable location simulation
	![image](scheme.png)
	* Hit `⌘+R` 
	
2. Things I'd do I'd have more time
    * Present more information to the user (min/max etc.)
    * Add an icon & splash image
    * Localize
    * Option to select city/point on the globe
	* Animated updates of the UI
	* Finish covering with unit tests
	* Add dynamic sizing to the collection view cells
	* Extract resuable code to a framework so we can use it easily on other projects
	* Download API key from the server for security 
	* Setup simple, free continuus integration
	* Add snapthots tests
	* Add automation tests
	

Tomek Kuźma
@mapedd